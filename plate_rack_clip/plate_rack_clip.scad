/*
# Clips for a plate rack

I bought an IKEA plate rack.  I want to screw it to the side of my cupboard and use it to hold pot lids.

(c) Richard Bowman 2020, released under CERN OHWL-S v2.

*/

use <../utilities.scad>;

clip_w = 10;
rack_d = 5;
screw_d = 4.5;
clip_h = rack_d + 2;
screw_profile = [[-1,screw_d], [rack_d, screw_d], [rack_d + 2, screw_d+4], [999, screw_d+4]];
roc = 2;
screw_y = rack_d/2 + screw_d/2 + 1;
$fn=32;

difference(){
    minkowski(){
        hull(){
            cube([clip_w - 2*roc, rack_d, 2*(clip_h - roc)], center=true);
            translate([0, screw_y, 0]) cylinder(d=clip_w-2*roc, h=clip_h-roc);
        }
        sphere(r=roc, $fn=16);
    }

    mirror([0,0,1]) cylinder(r=999, h=999, $fn=5); // the ground (or wall)

    hull() reflect([0,0,1]) translate([0,0,rack_d/2-0.3]) rotate([0,90,0]) cylinder(d=rack_d, h=999, center=true);

    translate([0,screw_y, 0]) for(i=[0:len(screw_profile)-2]){
        hull(){
            translate([0,0,screw_profile[i][0]]) cylinder(d=screw_profile[i][1], h=0.05);
            translate([0,0,screw_profile[i+1][0]]) cylinder(d=screw_profile[i+1][1], h=0.05);
        }
    }

}